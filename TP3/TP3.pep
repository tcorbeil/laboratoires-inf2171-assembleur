         LDX     0,i
lectAn:  CPX     4,i
         BRGE    verifAn
         CHARI   caract,d
         LDA     avCaract,d
         STBYTEA tabAnnee,x
         CPA     '\n',i
         BREQ    err
         CPA     '0',i
         BRLT    err
         CPA     '9',i
         BRGT    err
         SUBA    '0',i
         STA     temp,d
         LDA     annee,d
         ASLA
         ASLA    
         ADDA    annee,d
         ASLA
         ADDA    temp,d
         STA     annee,d
         ADDX    1,i
         BR      lectAn

verifAn: LDA     annee,d
         CPA     1901,i
         BRLT    err
         CPA     2050,i
         BRGT    err

         LDX     0,i
bclMois: CPX     2,i
         BRGE    verifMoi
         CHARI   caract,d
         LDA     avCaract,d
         STBYTEA tabMois,x
         CPA     '\n',i
         BREQ    err
         CPA     '0',i
         BRLT    err
         CPA     '9',i
         BRGT    err
         SUBA    '0',i
         STA     temp,d
         LDA     mois,d
         ASLA
         ASLA    
         ADDA    mois,d
         ASLA
         ADDA    temp,d
         STA     mois,d
         ADDX    1,i
         BR      bclMois

verifMoi:LDA     mois,d
         CPA     1,i
         BRLT    err 
         CPA     12,i
         BRGT    err 
         BR      affMois

affMois: LDX     mois,d
         SUBX    1,i
         ASLX  
         LDA     tabPtrM,x
         STA     adrMois,d 
         CHARO   ' ',i
         CHARO   ' ',i
         CHARO   ' ',i
         CHARO   ' ',i
         CHARO   ' ',i
         CHARO   ' ',i
         STRO    adrMois,n 
         CHARO   ' ',i
         LDX     0,i
         CHARO   tabAnnee,x
         LDX     1,i
         CHARO   tabAnnee,x
         LDX     2,i
         CHARO   tabAnnee,x
         LDX     3,i
         CHARO   tabAnnee,x
         CHARO   '\n',i
         CHARO   '\n',i
         CHARO   ' ',i
         CHARO   ' ',i
         CHARO   ' ',i
         CHARO   ' ',i
         CHARO   ' ',i
         CHARO   ' ',i

         CHARO   ' ',i
         CHARO   'D',i
         CHARO   ' ',i 

         CHARO   ' ',i
         CHARO   'L',i
         CHARO   ' ',i 

         CHARO   ' ',i
         CHARO   'M',i
         CHARO   ' ',i 

         CHARO   ' ',i
         CHARO   'M',i
         CHARO   ' ',i 

         CHARO   ' ',i
         CHARO   'J',i
         CHARO   ' ',i 

         CHARO   ' ',i
         CHARO   'V',i
         CHARO   ' ',i 

         CHARO   ' ',i
         CHARO   'S',i 

         LDA     annee,d
         SUBA    1900,i
         STA     annee,d
         LDX     365,i
         CALL    multi
         STA     jourAbs,d

         LDA     annee,d
         SUBA    1,i
         LDX     4,i
         CALL    divis
         ADDX    jourAbs,d
         STX     jourAbs,d
         
         LDA     mois,d
         SUBA    1,i
         LDX     57,i
         CALL    multi
         ADDA    50,i
         LDX     100,i
         CALL    divis
         ADDX    jourAbs,d
         STX     jourAbs,d

         LDA     mois,d
         SUBA    1,i
         LDX     30,i
         CALL    multi
         ADDA    jourAbs,d
         ADDA    1,i
         STA     jourAbs,d

         LDX     jourAbs,d
         LDA     annee,d      ; analysons le nombre d'ann�e depuis 1900
         ANDA    3,i         ; divisible par 4 ?
         BREQ    bissex      ; oui, on enl�ve 1
nonbisse:SUBX    1,i         ; -1 pour l'ann�e non bissextile 
bissex:  SUBX    1,i         ; -2 pour l'ann�e non bissextile ou -1 pour bissextile
         STX     jourAbs,d
        LDA      5,i
         ADDA    '0',i
         STBYTEA caract,d
         CHARO   caract,d
         BR      fin

err:     STRO    msgErr,d

fin:     STOP

;###################
; A = multiplicande
; X = multiplicateur
mucand:  .EQUATE 0;#2d
;###################
multi:   SUBSP   2,i;#mucand
         STA     mucand,s
         LDA     0,i
loopm:   CPX     0,i
         BRLE    retMulti
         ADDA    mucand,s
         SUBX    1,i
         BR      loopm
retMulti:RET2;#mucand 

;###################
; A = dividende
; X = diviseur
divisr:  .EQUATE 0;#2d
; A = reste
; X = resultat
;###################
divis:   SUBSP   2,i;#divisr
         STX     divisr,s
         LDX     0,i 
loopd:   CPA     0,i
         BRLT    finMulti
         SUBA    divisr,s
         ADDX    1,i
         BR      loopd
finMulti:SUBX    1,i
         ADDA    divisr,s
         RET2;#divisr
         

avCaract:.BLOCK  1
caract:  .BLOCK  1
temp:    .BLOCK  2           ;#2d
jourAbs: .BLOCK  2           ;#2d
annee:   .BLOCK  2           ;#2d
mois:    .BLOCK  2           ;#2d
tabAnnee:.BLOCK  4
tabMois: .BLOCK  2
msgErr:  .ASCII "Erreur: mauvais format!\x00"
adrMois: .BLOCK  2
tabPtrM: .ADDRSS JA
         .ADDRSS FE
         .ADDRSS MR
         .ADDRSS AP
         .ADDRSS MY
         .ADDRSS JN
         .ADDRSS JL
         .ADDRSS AU
         .ADDRSS SE
         .ADDRSS OC
         .ADDRSS NV
         .ADDRSS DE
JA:      .ASCII "    Janvier\x00"
FE:      .ASCII "    F�vrier\x00"
MR:      .ASCII "      Mars\x00"
AP:      .ASCII "     Avril\x00"
MY:      .ASCII "      Mai\x00"
JN:      .ASCII "      Juin\x00"
JL:      .ASCII "    Juillet\x00"
AU:      .ASCII "      Ao�t\x00"
SE:      .ASCII "   Septembre\x00"
OC:      .ASCII "    Octobre\x00"
NV:      .ASCII "    Novembre\x00"
DE:      .ASCII "    D�cembre\x00"
.END
