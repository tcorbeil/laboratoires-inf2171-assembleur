# Laboratoires-INF2171-Assembleur

Solution des laboratoires pour le cours INF2171 à la session automne 2020

## Solutions des laboratoires

1. Semaine 3: [Traitement des nombres avec DECI](https://gitlab.com/tcorbeil/laboratoires-inf2171-assembleur/-/blob/master/LAB_3.zip)
2. Semaine 4: [Traitement de tableaux ASCII](https://gitlab.com/tcorbeil/laboratoires-inf2171-assembleur/-/blob/master/LAB_4.zip) 
3. Semaine 9: [TP2 partie 1](https://gitlab.com/tcorbeil/laboratoires-inf2171-assembleur/-/blob/master/TP2_P1.pep)
4. Semaine 10: [TP2 partie 2](https://gitlab.com/tcorbeil/laboratoires-inf2171-assembleur/-/blob/master/TP2_P2.pep) 
5. Semaine 1: [TP2 partie 3](https://gitlab.com/tcorbeil/laboratoires-inf2171-assembleur/-/blob/master/TP2_P3) `MISE À JOUR`

## Slack

J'ai créé un canal sur le slack de l'AGEEI afin de pouvoir répondre aux
questions à propos des TP ou autres. Il suffit de chercher `inf2171-h2020-démo-tc`
dans la section `Accéder à` de Slack (en haut à gauche).