bclLect: CHARI   caract,d
         LDA     avCaract,d
         CPA     '\n',i
         BREQ    affVoy
         ORA     32,i
         CPA     'a',i
         BRLT    bclLect 
         CPA     'z',i 
         BRGT    bclLect

         SUBSP   4,i;#sauvX #sauvA 
         STA     sauvA,s
         STX     sauvX,s
         SUBSP   2,i;#caractLu 
         STA     0,s
         CALL    cptCar
         ADDSP   2,i;#caractLu
         LDA     sauvA,s
         LDX     sauvX,s
         ADDSP   4,i;#sauvX #sauvA

         BR      bclLect

affVoy:  SUBSP   4,i;#sauvX #sauvA 
         STA     sauvA,s
         STX     sauvX,s
         SUBSP   2,i;#indVoyCp
         LDA     0,i
         STA     0,s
         CALL    voyComp
         LDA     8,i
         STA     0,s
         CALL    voyComp 
         LDA     16,i
         STA     0,s
         CALL    voyComp
         LDA     28,i
         STA     0,s
         CALL    voyComp
         LDA     40,i
         STA     0,s
         CALL    voyComp
         LDA     48,i
         STA     0,s
         CALL    voyComp
         ADDSP   2,i;#indVoyCp 
         LDA     sauvA,s
         LDX     sauvX,s
         ADDSP   4,i;#sauvA #sauvX 

         LDX     indVoy,d
         CPX     -1,i
         BREQ    calclPct
         STRO    msgVoy1,d
         DECO    maxVoy,d 
         STRO    msgVoy2,d
         LDA     indVoy,d
         ASRA    
         ADDA    'A',i
         STBYTEA caract,d
         CHARO   caract,d
         STRO    msgVoy3,d

calclPct:LDA     0,i
         LDX     0,i
loopNbL: CPX     52,i
         BRGT    finLoopL
         ADDA    comptCar,x
         ADDX    2,i
         BR      loopNbL
finLoopL:STA     nbLtr,d

         LDA     0,i;a=0 e=4*2=8 i=8*2=16 o=14*28 u=20*2=40 y=24*48
         LDX     0,i
         ADDA    comptCar,x
         LDX     8,i
         ADDA    comptCar,x
         LDX     16,i
         ADDA    comptCar,x
         LDX     28,i
         ADDA    comptCar,x
         LDX     40,i
         ADDA    comptCar,x
         LDX     48,i
         ADDA    comptCar,x
         STA     nbVoy,d 

         SUBSP   8,i;#reste #res #divis #divid
         LDA     nbVoy,d
         ASLA
         ASLA    
         ADDA    nbVoy,d
         ASLA    
         STA     nbVoy,d
         ASLA
         ASLA    
         ADDA    nbVoy,d
         ASLA   
         STA     0,s 

         LDA     nbLtr,d 
         STA     2,s
         LDA     0,i

         call    srDivis

         LDA     4,s
         STA     avVirg,d  
         LDA     6,s  
         STA     varTrav,d
         ASLA
         ASLA
         ADDA    varTrav,d
         ASLA
         STA     varTrav,d
         ASLA    
         ASLA    
         ADDA    varTrav,d
         ASLA    
         STA     0,s
         LDA     nbLtr,d
         STA     2,s

         CALL    srDivis

         LDA     4,s
         STA     apVirg,d
         LDA     6,s
         ASLA    
         CPA     nbLtr,d
         BRLT    sautP1
         LDA     apVirg,d
         ADDA    1,i
         STA     apVirg,d
sautP1:  ADDSP   8,i         ;#reste #res #divis #divid

         DECO    avVirg,d
         LDA     apVirg,d
         BREQ    fin
         CHARO   ',',i
         DECO    apVirg,d
         

fin:     STOP

;#################################
caractLu:.EQUATE 2;#2d
;#################################
cptCar:  LDX     caractLu,s
         SUBX    'a',i
         ASLX
         LDA     comptCar,x 
         ADDA    1,i
         STA     comptCar,x
         RET0

;#################################
indVoyCp:.EQUATE 2;#2d
;#################################
voyComp: LDX     indVoyCp,s
         LDA     comptCar,x
         CPA     maxVoy,d
         BRLE    sautMax
         STA     maxVoy,d 
         STX     indVoy,d
sautMax: RET0    

;#################################
divid:   .EQUATE 2;#2d
divis:   .EQUATE 4;#2d
res:     .EQUATE 6;#2d
reste:   .EQUATE 8;#2d
;#################################
srDivis: LDA     divid,s
         LDX     0,i
loopdiv: CPA     0,i
         BRLT    findiv
         SUBA    divis,s
         ADDX    1,i
         BR      loopdiv
findiv:  ADDA    divis,s
         SUBX    1,i
         STA     reste,s
         STX     res,s
         RET0
  
sauvA:   .EQUATE 0;#2d
sauvX:   .EQUATE 2;#2d
msgVoy1: .ASCII  "Avec \x00"
msgVoy2: .ASCII  " occurences, la voyelle\" \x00"
msgVoy3: .ASCII  "\" est la plus utilis�e,\n\x00" 
avVirg:  .BLOCK  2
apVirg:  .BLOCK  2
varTrav: .BLOCK  2
avCaract:.BLOCK  1
caract:  .BLOCK  1
maxVoy:  .BLOCK  2
indVoy:  .WORD   -1
nbLtr:   .BLOCK  2
nbVoy:   .BLOCK  2	
comptCar:.BLOCK  52
         .END











